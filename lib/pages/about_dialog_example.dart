import 'package:flutter/material.dart';

class AboutDialogExample extends StatefulWidget {
  final String title;

  AboutDialogExample(this.title);

  @override
  _AboutDialogExampleState createState() => _AboutDialogExampleState();
}

class _AboutDialogExampleState extends State<AboutDialogExample> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: ElevatedButton(
            onPressed: () {
              showAboutDialog(
                context: context,
                applicationName: "Flutterer",
                applicationVersion: "1.2.3",
                applicationIcon: Icon(
                  Icons.flutter_dash_rounded,
                  size: 48,
                  color: Color(0xff5808e5),
                ),
                applicationLegalese:
                "Here in 'applicationLegalese' you can describe your app policies, terms and conditions",
                children: [
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      "I am optional children 1",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Text(
                      "I am optional children 2",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ],
                useRootNavigator: false,
              );
            },
            child: Text("Show About Dialog".toUpperCase()),
          )),
    );
  }
}
